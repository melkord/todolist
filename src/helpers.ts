import { TodoItem } from './types'

export const toggleItemStatus = (
  items: TodoItem[],
  itemTimestamp: number,
): TodoItem[] =>
  items.map(item => {
    if (item.creationTimestamp === itemTimestamp) {
      const toggledStatus =
        item.status === 'completed' ? 'pending' : 'completed'
      return {
        ...item,
        status: toggledStatus,
      }
    } else {
      return item
    }
  })

export const orderItems = (items: TodoItem[]): TodoItem[] => {
  const { pending, completed } = items.reduce<{
    pending: TodoItem[]
    completed: TodoItem[]
  }>(
    (acc, current) => {
      const c =
        current.status === 'completed'
          ? {
              pending: acc.pending,
              completed: [...acc.completed, current],
            }
          : {
              completed: acc.completed,
              pending: [...acc.pending, current],
            }
      return c
    },
    { pending: [], completed: [] },
  )

  const sortByTimestamp = (itemA: TodoItem, itemB: TodoItem) =>
    itemB.creationTimestamp - itemA.creationTimestamp

  return [...pending.sort(sortByTimestamp), ...completed]
}
