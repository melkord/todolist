import { render, screen, fireEvent } from '@testing-library/react'
import App from './App'
import { paste } from '@testing-library/user-event/dist/paste'

const getInputContainer = () =>
  screen.getByPlaceholderText(/write your todo item/i)

const getEmptyStateContainer = () => screen.getByText(/no todo added yet/i)

const getAddButtonContainer = () =>
  screen.getByRole('button', { name: /add the todo item/i })

describe('App', () => {
  it('should renders todo list empty state', () => {
    render(<App />)
    expect(getEmptyStateContainer()).toBeInTheDocument()
    expect(getInputContainer()).toBeInTheDocument()
  })

  it('should add item items', async () => {
    render(<App />)
    expect(getEmptyStateContainer()).toBeInTheDocument()

    paste(getInputContainer(), 'foo todo item')
    fireEvent.click(getAddButtonContainer())

    paste(getInputContainer(), 'bar todo item')
    fireEvent.keyDown(getInputContainer(), {
      key: 'Enter',
    })

    expect(screen.queryByText(/no todo added yet/i)).not.toBeInTheDocument()
    expect(
      screen.getByRole('checkbox', { name: /foo todo item/i }),
    ).toBeInTheDocument()
    expect(
      screen.getByRole('checkbox', { name: /bar todo item/i }),
    ).toBeInTheDocument()
  })

  it('should set completed when a todo item is clicked', () => {
    render(<App />)

    paste(getInputContainer(), 'foo todo item')
    fireEvent.click(getAddButtonContainer())

    paste(getInputContainer(), 'bar todo item')
    fireEvent.click(getAddButtonContainer())

    fireEvent.click(screen.getByRole('checkbox', { name: /foo todo item/i }))

    expect(
      screen.getByRole('checkbox', { name: /foo todo item/i }),
    ).toBeChecked()
    expect(
      screen.getByRole('checkbox', { name: /bar todo item/i }),
    ).not.toBeChecked()
  })

  it('should set pending when a completed todo item is clicked', () => {
    render(<App />)

    paste(getInputContainer(), 'foo todo item')
    fireEvent.click(getAddButtonContainer())

    paste(getInputContainer(), 'bar todo item')
    fireEvent.click(getAddButtonContainer())

    fireEvent.click(screen.getByRole('checkbox', { name: /foo todo item/i }))

    expect(
      screen.getByRole('checkbox', { name: /foo todo item/i }),
    ).toBeChecked()
    expect(
      screen.getByRole('checkbox', { name: /bar todo item/i }),
    ).not.toBeChecked()

    fireEvent.click(screen.getByRole('checkbox', { name: /foo todo item/i }))
    expect(
      screen.getByRole('checkbox', { name: /foo todo item/i }),
    ).not.toBeChecked()
    expect(
      screen.getByRole('checkbox', { name: /bar todo item/i }),
    ).not.toBeChecked()
  })

  describe('Validation', () => {
    it('should show validation message when input is empty and enter pressed', () => {
      render(<App />)
      fireEvent.keyDown(getInputContainer(), {
        key: 'Enter',
      })
      expect(
        screen.getByText(/the todo item is mandatory/i),
      ).toBeInTheDocument()
    })

    it('should show validation message when input is empty and add button is clicked', () => {
      render(<App />)
      fireEvent.click(getAddButtonContainer())
      expect(
        screen.getByText(/the todo item is mandatory/i),
      ).toBeInTheDocument()
    })
  })
})
