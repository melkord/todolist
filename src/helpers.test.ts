import { toggleItemStatus, orderItems } from './helpers'
import { TodoItem } from './types'

const items: TodoItem[] = [
  {
    name: 'foo',
    status: 'pending',
    creationTimestamp: 1111,
  },
  {
    name: 'bar',
    status: 'completed',
    creationTimestamp: 2222,
  },
]

describe('helers', () => {
  describe('toggleItemStatus', () => {
    it('should set as completed the element', () => {
      expect(toggleItemStatus(items, 1111)).toMatchObject([
        {
          name: 'foo',
          status: 'completed',
          creationTimestamp: 1111,
        },
        {
          name: 'bar',
          status: 'completed',
          creationTimestamp: 2222,
        },
      ])
    })
    it('should set as pending the element', () => {
      expect(toggleItemStatus(items, 2222)).toMatchObject([
        {
          name: 'foo',
          status: 'pending',
          creationTimestamp: 1111,
        },
        {
          name: 'bar',
          status: 'pending',
          creationTimestamp: 2222,
        },
      ])
    })
    it('should do nothing if not existing creationTimestamp is used', () => {
      expect(toggleItemStatus(items, 333)).toMatchObject([
        {
          name: 'foo',
          status: 'pending',
          creationTimestamp: 1111,
        },
        {
          name: 'bar',
          status: 'completed',
          creationTimestamp: 2222,
        },
      ])
    })
  })
  describe('orderItems', () => {
    it('should order the items properly', () => {
      const itemsToOrder: TodoItem[] = [
        {
          name: 'bar 7',
          status: 'completed',
          creationTimestamp: 7777,
        },
        {
          name: 'foo 2',
          status: 'pending',
          creationTimestamp: 2222,
        },
        {
          name: 'bar 6',
          status: 'completed',
          creationTimestamp: 6666,
        },
        {
          name: 'foo 4',
          status: 'pending',
          creationTimestamp: 4444,
        },
        {
          name: 'foo 3',
          status: 'pending',
          creationTimestamp: 3333,
        },
        {
          name: 'foo 1',
          status: 'pending',
          creationTimestamp: 1111,
        },
        {
          name: 'bar 5',
          status: 'completed',
          creationTimestamp: 5555,
        },
      ]

      expect(orderItems(itemsToOrder)).toMatchObject([
        {
          name: 'foo 4',
          status: 'pending',
          creationTimestamp: 4444,
        },
        {
          name: 'foo 3',
          status: 'pending',
          creationTimestamp: 3333,
        },
        {
          name: 'foo 2',
          status: 'pending',
          creationTimestamp: 2222,
        },
        {
          name: 'foo 1',
          status: 'pending',
          creationTimestamp: 1111,
        },
        {
          name: 'bar 7',
          status: 'completed',
          creationTimestamp: 7777,
        },
        {
          name: 'bar 6',
          status: 'completed',
          creationTimestamp: 6666,
        },
        {
          name: 'bar 5',
          status: 'completed',
          creationTimestamp: 5555,
        },
      ])
    })
  })
})
