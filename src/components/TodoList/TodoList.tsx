import { TodoItem } from '../../types'

const TodoList = ({
  items,
  onItemClick,
}: {
  items: TodoItem[]
  onItemClick: (itemTimestamp: number) => void
}) => (
  <ul className="todo-list">
    {items.map(item => (
      <li key={item.creationTimestamp} className={`item-status-${item.status}`}>
        <input
          type="checkbox"
          checked={item.status === 'completed'}
          onChange={() => {
            onItemClick(item.creationTimestamp)
          }}
          id={`item-id-${item.creationTimestamp}`}
        />
        <label htmlFor={`item-id-${item.creationTimestamp}`}>{item.name}</label>
      </li>
    ))}
  </ul>
)

export default TodoList
