import { useCallback, useState } from 'react'

const TodoItemAdder = ({
  onItemAdd,
  hasError,
}: {
  onItemAdd: (newItem: string) => void
  hasError: boolean
}) => {
  const [newItem, setNewItem] = useState<string>('')

  const handleItemAdd = useCallback(() => {
    onItemAdd(newItem)
  }, [newItem, onItemAdd])

  return (
    <div>
      <input
        placeholder="Write your todo item"
        type="text"
        onChange={e => {
          setNewItem(e.target.value)
        }}
        value={newItem}
        required
        onKeyDown={e => {
          if (e.key === 'Enter') {
            handleItemAdd()
          }
        }}
        aria-required="true"
        className={hasError ? 'has-error' : ''}
        autoFocus
      />
      <button
        onClick={() => {
          handleItemAdd()
        }}
        aria-label="Add the todo item"
      >
        +
      </button>
      {hasError && (
        <div className="validation-error">The todo item is mandatory</div>
      )}
    </div>
  )
}

export default TodoItemAdder
