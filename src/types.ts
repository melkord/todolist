export type TodoItem = {
  name: string
  status: 'pending' | 'completed'
  creationTimestamp: number
}
