import Header from './components/Header'
import TodoList from './components/TodoList'
import TodoItemAdder from './components/TodoItemAdder'

import useTodoList from './hooks/useTodoList'

const App = () => {
  const { items, onItemAdd, onItemClick, hasError } = useTodoList()

  return (
    <div className="app">
      <Header />
      {items.length > 0 && <TodoList items={items} onItemClick={onItemClick} />}
      {items.length === 0 && (
        <div className="empty-state">No todo added yet</div>
      )}
      <TodoItemAdder
        onItemAdd={onItemAdd}
        hasError={hasError}
        key={items.length}
      />
    </div>
  )
}

export default App
