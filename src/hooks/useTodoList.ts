import { useMemo, useReducer } from 'react'
import { orderItems, toggleItemStatus } from '../helpers'
import { TodoItem } from '../types'

type State = {
  items: TodoItem[]
  hasError: boolean
}

type Action =
  | {
      type: 'add-item'
      name?: string
    }
  | {
      type: 'toggle-item-status'
      creationTimestamp: number
    }

const initialState: State = { items: [], hasError: false }

const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case 'add-item':
      return action.name && action.name !== ''
        ? {
            ...state,
            hasError: false,
            items: [
              ...state.items,
              {
                name: action.name,
                status: 'pending' as const,
                creationTimestamp: Date.now(),
              },
            ],
          }
        : { ...state, hasError: true }

    case 'toggle-item-status':
      return {
        ...state,
        items: toggleItemStatus(state.items, action.creationTimestamp),
      }
    default:
      throw new Error()
  }
}

const useTodoList = () => {
  const [state, dispatch] = useReducer(reducer, initialState)

  const orderedItems = useMemo(() => orderItems(state.items), [state.items])

  return {
    items: orderedItems,
    onItemClick: (creationTimestamp: number) =>
      dispatch({ type: 'toggle-item-status', creationTimestamp }),
    onItemAdd: (newItemName?: string) =>
      dispatch({ type: 'add-item', name: newItemName }),
    hasError: state.hasError,
  }
}

export default useTodoList
